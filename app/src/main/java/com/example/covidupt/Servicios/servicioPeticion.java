package com.example.covidupt.Servicios;





import com.example.covidupt.ViewModels.Countries;
import com.example.covidupt.ViewModels.Day_one;
import com.example.covidupt.ViewModels.Day_one_all_status;
import com.example.covidupt.ViewModels.Peticion_ayuda_Mexico;
import com.example.covidupt.ViewModels.Peticion_resumen;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface servicioPeticion {

    //resumen fragment
    @GET("summary")
    Call<Peticion_resumen>Resumen();

    //paises peticion
    @GET("countries")
    Call<List<Countries>>Paises();

    //Casos confirmados fragment
    @GET("dayone/country/{country}/status/confirmed")
    Call<List<Day_one>>Casos_confirmados_dia_uno(@Path("country") String country);

    //todos los casos fragment
    @GET("dayone/country/{country}")
    Call<List<Day_one_all_status>>Todos_los_casos(@Path("country") String country);

    //planes de ayuda mexico
    @GET("search//?dataset=inventario-de-acciones-y-programas-sociales-rendicion-de-cuentas&rows=300")
    Call<Peticion_ayuda_Mexico>Ayuda_Mexico();

}
