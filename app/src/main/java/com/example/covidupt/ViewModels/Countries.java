package com.example.covidupt.ViewModels;

public class Countries {
    public String Country;
    public String ISO2;

    public Countries(String country) {
        Country = country;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getISO2() {
        return ISO2;
    }

    public void setISO2(String ISO2) {
        this.ISO2 = ISO2;
    }
}
