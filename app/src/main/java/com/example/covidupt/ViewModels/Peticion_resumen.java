package com.example.covidupt.ViewModels;



import java.util.List;

public class Peticion_resumen {
    private Global Global;
    private List<Country> Countries;
    private String Date;

    public Global getGlobal() {
        return Global;
    }

    public void setGlobal(Global global) {
        this.Global = global;
    }


    public List<Country> getCountries() {
        return Countries;
    }

    public void setCountries(List<Country> countries) {
        this.Countries = countries;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        this.Date = date;
    }
}
