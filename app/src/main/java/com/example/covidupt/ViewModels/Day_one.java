package com.example.covidupt.ViewModels;

public class Day_one {
    public String Country;
    public int Cases;
    public String Status;
    public String Date;

    public Day_one(String country, int cases, String status, String date) {
        Country = country;
        Cases = cases;
        Status = status;
        Date = date;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public int getCases() {
        return Cases;
    }

    public void setCases(int cases) {
        Cases = cases;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }
}
