package com.example.covidupt.ViewModels;

import java.util.List;

public class Fields {
    public String fecha_publicacion_gaceta;
    public String descripcion_programa_accion_social;
    public String nombre_programa_accion_social;
    public String unidad_responsable;
    public String enlace_a_gaceta;


    public Fields( String fecha_publicacion_gaceta, String descripcion_programa_accion_social,  String nombre_programa_accion_social, String unidad_responsable) {
        this.fecha_publicacion_gaceta = fecha_publicacion_gaceta;
        this.descripcion_programa_accion_social = descripcion_programa_accion_social;
        this.nombre_programa_accion_social = nombre_programa_accion_social;
        this.unidad_responsable = unidad_responsable;
    }

    public String getFecha_publicacion_gaceta() {
        return fecha_publicacion_gaceta;
    }

    public void setFecha_publicacion_gaceta(String fecha_publicacion_gaceta) {
        this.fecha_publicacion_gaceta = fecha_publicacion_gaceta;
    }

    public String getDescripcion_programa_accion_social() {
        return descripcion_programa_accion_social;
    }

    public void setDescripcion_programa_accion_social(String descripcion_programa_accion_social) {
        this.descripcion_programa_accion_social = descripcion_programa_accion_social;
    }

    public String getNombre_programa_accion_social() {
        return nombre_programa_accion_social;
    }

    public void setNombre_programa_accion_social(String nombre_programa_accion_social) {
        this.nombre_programa_accion_social = nombre_programa_accion_social;
    }

    public String getUnidad_responsable() {
        return unidad_responsable;
    }

    public void setUnidad_responsable(String unidad_responsable) {
        this.unidad_responsable = unidad_responsable;
    }

    public String getEnlace_a_gaceta() {
        return enlace_a_gaceta;
    }

    public void setEnlace_a_gaceta(String enlace_a_gaceta) {
        this.enlace_a_gaceta = enlace_a_gaceta;
    }
}
