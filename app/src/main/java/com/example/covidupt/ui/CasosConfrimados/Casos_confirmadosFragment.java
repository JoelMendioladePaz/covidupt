package com.example.covidupt.ui.CasosConfrimados;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.covidupt.Adaptadores.Adaptadorcasosconfrimados;
import com.example.covidupt.Adaptadores.Adaptadorspinnerpaises;
import com.example.covidupt.Api.Api;
import com.example.covidupt.R;
import com.example.covidupt.Servicios.servicioPeticion;
import com.example.covidupt.ViewModels.Countries;
import com.example.covidupt.ViewModels.Day_one;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Casos_confirmadosFragment extends Fragment {

    private ConfirmadosViewModel confirmadosViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        confirmadosViewModel =
                ViewModelProviders.of(this).get(ConfirmadosViewModel.class);
        View root = inflater.inflate(R.layout.fragment_gallery, container, false);
        confirmadosViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
            }
        });
        return root;
    }

    private Spinner spinner;
    private Adaptadorspinnerpaises adaptadorspinnerpaises;
    private ArrayList<Countries> listapaises=new ArrayList<>();
    private ArrayList<Day_one> listacasos=new ArrayList<>();
    private Adaptadorcasosconfrimados adaptadorcasosconfrimados;
    private ListView listacasosconfirmados;
    private ArrayList<String> listacodigopais=new ArrayList<>();
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        spinner=view.findViewById(R.id.spinnertodosloscasos);
        listacasosconfirmados=view.findViewById(R.id.listacasosconfirmados);
        llenarspinner();
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(listacodigopais.size()>0){
                    listacasosconfirmados.setAdapter(null);
                    String pais=listacodigopais.get(i);
                    obtenerItems(pais);
                }

            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void llenarspinner() {
        servicioPeticion service = Api.getApi(this.getActivity()).create(servicioPeticion.class);
        Call<List<Countries>> countriesCall=service.Paises();
        countriesCall.enqueue(new Callback<List<Countries>>() {
            @Override
            public void onResponse(Call<List<Countries>> call, Response<List<Countries>> response) {
                List<Countries> peticion= response.body();
                if(response.body()==null){
                    Toast.makeText(getContext(),"Ocurrio un error, intentalo más tarde",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(peticion.size()>1){
                    for (int i=0;i<peticion.size();i++){
                        listapaises.add(new Countries(peticion.get(i).Country));
                        listacodigopais.add(peticion.get(i).ISO2);
                    }
                    adaptadorspinnerpaises=new Adaptadorspinnerpaises(listapaises,getContext());
                    spinner.setAdapter(adaptadorspinnerpaises);
                }else
                    Toast.makeText(getContext(),"Error",Toast.LENGTH_LONG).show();

            }

            @Override
            public void onFailure(Call<List<Countries>> call, Throwable t) {
                Toast.makeText(getContext(), "Error en el servidor", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void obtenerItems(final String pais){
        servicioPeticion service = Api.getApi(this.getActivity()).create(servicioPeticion.class);
        Call<List<Day_one>> daycall=service.Casos_confirmados_dia_uno(pais);
        daycall.enqueue(new Callback<List<Day_one>>() {
            @Override
            public void onResponse(Call<List<Day_one>> call, Response<List<Day_one>> response) {
                List<Day_one> peticion= response.body();
                if(response.body()==null){
                    Toast.makeText(getContext(),"Ocurrio un error, intentalo más tarde",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(peticion.size()>1){
                    listacasos.clear();
                    for (int i=0;i<peticion.size();i++){
                        listacasos.add(new Day_one(peticion.get(i).Country,peticion.get(i).Cases,peticion.get(i).Status,peticion.get(i).Date));
                    }
                    adaptadorcasosconfrimados=new Adaptadorcasosconfrimados(listacasos,getContext());
                    listacasosconfirmados.setAdapter(adaptadorcasosconfrimados);
                }else
                    Toast.makeText(getContext(),"Parece que aun no tenemos información",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<List<Day_one>> call, Throwable t) {
                Toast.makeText(getContext(), "Error en el servidor", Toast.LENGTH_SHORT).show();
            }
        });
    }
}