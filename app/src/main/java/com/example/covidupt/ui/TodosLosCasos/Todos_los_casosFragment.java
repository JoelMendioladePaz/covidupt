package com.example.covidupt.ui.TodosLosCasos;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.covidupt.Adaptadores.Adaptadorcasosconfrimados;
import com.example.covidupt.Adaptadores.Adaptadorspinnerpaises;
import com.example.covidupt.Adaptadores.Adaptadortodosloscasos;
import com.example.covidupt.Api.Api;
import com.example.covidupt.R;
import com.example.covidupt.Servicios.servicioPeticion;
import com.example.covidupt.ViewModels.Countries;
import com.example.covidupt.ViewModels.Day_one;
import com.example.covidupt.ViewModels.Day_one_all_status;

import java.util.ArrayList;
import java.util.List;

import lecho.lib.hellocharts.model.PieChartData;
import lecho.lib.hellocharts.model.SliceValue;
import lecho.lib.hellocharts.view.PieChartView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Todos_los_casosFragment extends Fragment {

    private TodosViewModel todosViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        todosViewModel =
                ViewModelProviders.of(this).get(TodosViewModel.class);
        View root = inflater.inflate(R.layout.fragment_slideshow, container, false);
        todosViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
            }
        });
        return root;
    }
    private Spinner spinner;
    private Adaptadorspinnerpaises adaptadorspinnerpaises;
    private ArrayList<Countries> listapaises=new ArrayList<>();
    private ArrayList<String> listacodigopais=new ArrayList<>();
    private ArrayList<Day_one_all_status> listacasos=new ArrayList<>();
    private ListView listatodoscasos;
    private Adaptadortodosloscasos adaptadortodosloscasos;
    private PieChartView pieChartView;
    private List pieData = new ArrayList<>();
    private PieChartData pieChartData;
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        llenarspinner();
        spinner=view.findViewById(R.id.spinnertodosloscasos);
        listatodoscasos=view.findViewById(R.id.listatodosloscasos);
        pieChartView=view.findViewById(R.id.todosloscasosgrafica);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(listacodigopais.size()>0){
                    listatodoscasos.setAdapter(null);
                    pieChartView.setPieChartData(null);
                    String pais=listacodigopais.get(i);
                    obtenerItems(pais);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }
    private void llenarspinner() {
        servicioPeticion service = Api.getApi(this.getActivity()).create(servicioPeticion.class);
        Call<List<Countries>> countriesCall=service.Paises();
        countriesCall.enqueue(new Callback<List<Countries>>() {
            @Override
            public void onResponse(Call<List<Countries>> call, Response<List<Countries>> response) {
                List<Countries> peticion= response.body();
                if(response.body()==null){
                    Toast.makeText(getContext(),"Ocurrio un error, intentalo más tarde",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(peticion.size()>1){
                    for (int i=0;i<peticion.size();i++){
                        listapaises.add(new Countries(peticion.get(i).Country));
                        listacodigopais.add(peticion.get(i).ISO2);
                    }
                    adaptadorspinnerpaises=new Adaptadorspinnerpaises(listapaises,getContext());
                    spinner.setAdapter(adaptadorspinnerpaises);
                }else
                    Toast.makeText(getContext(),"Error",Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<List<Countries>> call, Throwable t) {
                Toast.makeText(getContext(), "Error en el servidor", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void obtenerItems(String pais){
        servicioPeticion service = Api.getApi(this.getActivity()).create(servicioPeticion.class);
        Call<List<Day_one_all_status>> day_onecall=service.Todos_los_casos(pais);
        day_onecall.enqueue(new Callback<List<Day_one_all_status>>() {
            @Override
            public void onResponse(Call<List<Day_one_all_status>> call, Response<List<Day_one_all_status>> response) {
                List<Day_one_all_status> peticion= response.body();
                if(response.body()==null){
                    Toast.makeText(getContext(),"Ocurrio un error, intentalo más tarde",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(peticion.size()>1){
                    listacasos.clear();
                    pieData.clear();
                    for (int i=peticion.size()-1;i>0;i--){
                        listacasos.add(new Day_one_all_status(peticion.get(i).Country,peticion.get(i).Confirmed,peticion.get(i).Deaths,peticion.get(i).Recovered,peticion.get(i).Active,peticion.get(i).Date));

                    }
                    pieData.add(new SliceValue(peticion.get(peticion.size()-1).Confirmed, Color.BLUE).setLabel("Confirmados: \n\r"+peticion.get(peticion.size()-1).Confirmed));
                    pieData.add(new SliceValue(peticion.get(peticion.size()-1).Deaths, Color.RED).setLabel("Muertes: \n\r"+peticion.get(peticion.size()-1).Deaths));
                    pieData.add(new SliceValue(peticion.get(peticion.size()-1).Recovered, Color.MAGENTA).setLabel("Rehabilitados: \n\r"+peticion.get(peticion.size()-1).Recovered));
                    pieData.add(new SliceValue(peticion.get(peticion.size()-1).Active, Color.BLACK).setLabel("Hospitalizados: \n\r"+peticion.get(peticion.size()-1).Active));
                    pieChartData= new PieChartData(pieData);
                    pieChartData.setHasLabels(true).setValueLabelTextSize(14);
                    pieChartData.setHasCenterCircle(true).setCenterText1("estado actual").setCenterText1FontSize(20).setCenterText1Color(Color.parseColor("#0097A7"));
                    pieChartView.setPieChartData(pieChartData);
                    adaptadortodosloscasos=new Adaptadortodosloscasos(listacasos,getContext());
                    listatodoscasos.setAdapter(adaptadortodosloscasos);
                }else
                    Toast.makeText(getContext(),"Parece que aun no tenemos información",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<List<Day_one_all_status>> call, Throwable t) {

            }
        });

    }

}