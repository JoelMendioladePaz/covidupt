package com.example.covidupt.ui.ResumenGlobal;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class ResumenViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public ResumenViewModel() {
        mText = new MutableLiveData<>();
    }

}