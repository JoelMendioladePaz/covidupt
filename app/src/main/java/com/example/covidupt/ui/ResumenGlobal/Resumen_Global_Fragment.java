package com.example.covidupt.ui.ResumenGlobal;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.covidupt.Adaptadores.ResumenAdaptador;
import com.example.covidupt.Adaptadores.ResumenAdaptadorPais;
import com.example.covidupt.Api.Api;
import com.example.covidupt.R;
import com.example.covidupt.Servicios.servicioPeticion;
import com.example.covidupt.ViewModels.Country;
import com.example.covidupt.ViewModels.Global;
import com.example.covidupt.ViewModels.Peticion_resumen;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Resumen_Global_Fragment extends Fragment {

    private ResumenViewModel resumenViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        resumenViewModel =
                ViewModelProviders.of(this).get(ResumenViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        return root;
    }


    private ListView lista,listadepaises;
    private ResumenAdaptador adaptador;
    private ResumenAdaptadorPais adaptadorPais;
    private ArrayList<Global>listaitems=new ArrayList<>();
    private ArrayList<Country> listaPaises=new ArrayList<>();
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        lista=view.findViewById(R.id.listaResumen);
        listadepaises=view.findViewById(R.id.listaResumenPorPais);
        obtenerItems();

    }
    private void obtenerItems(){
        servicioPeticion service = Api.getApi(this.getActivity()).create(servicioPeticion.class);
        Call<Peticion_resumen> resumenCall=service.Resumen();
        resumenCall.enqueue(new Callback<Peticion_resumen>() {
            @Override
            public void onResponse(Call<Peticion_resumen> call, Response<Peticion_resumen> response) {
                Peticion_resumen peticion=response.body();
                if(response.body()==null){
                    Toast.makeText(getContext(),"Ocurrio un error, intentalo más tarde",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(peticion.getGlobal()!=null){
                    Limpiar();
                    listaitems.add(new Global(peticion.getGlobal().NewConfirmed,peticion.getGlobal().TotalConfirmed,peticion.getGlobal().NewDeaths,peticion.getGlobal().TotalDeaths,peticion.getGlobal().NewRecovered,peticion.getGlobal().TotalRecovered));
                    adaptador=new ResumenAdaptador(listaitems,getContext());
                    lista.setAdapter(adaptador);
                    for (int i=0;i<peticion.getCountries().size();i++){
                        listaPaises.add(new Country(peticion.getCountries().get(i).Country,peticion.getCountries().get(i).NewConfirmed,peticion.getCountries().get(i).TotalConfirmed,peticion.getCountries().get(i).NewDeaths,peticion.getCountries().get(i).TotalDeaths,peticion.getCountries().get(i).NewRecovered,peticion.getCountries().get(i).TotalRecovered,peticion.getCountries().get(i).Date));
                    }
                    adaptadorPais=new ResumenAdaptadorPais(listaPaises,getContext());
                    listadepaises.setAdapter(adaptadorPais);
                }else
                    Toast.makeText(getContext(),"Error",Toast.LENGTH_SHORT).show();
                }

            @Override
            public void onFailure(Call<Peticion_resumen> call, Throwable t) {
                Toast.makeText(getContext(),"No podemos conectarnos :c", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void Limpiar() {
        listaitems.clear();
        lista.setAdapter(null);
        listaPaises.clear();
        listadepaises.setAdapter(null);
    }
}