package com.example.covidupt.ui.CasosConfrimados;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class ConfirmadosViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public ConfirmadosViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is gallery fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}