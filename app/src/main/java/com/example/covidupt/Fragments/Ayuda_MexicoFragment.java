package com.example.covidupt.Fragments;

import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.covidupt.Adaptadores.Adaptadorayudamexico;
import com.example.covidupt.Adaptadores.ResumenAdaptador;
import com.example.covidupt.Adaptadores.ResumenAdaptadorPais;
import com.example.covidupt.Api.Api;
import com.example.covidupt.Api.Api2;
import com.example.covidupt.R;
import com.example.covidupt.Servicios.servicioPeticion;
import com.example.covidupt.ViewModels.Country;
import com.example.covidupt.ViewModels.Fields;
import com.example.covidupt.ViewModels.Global;
import com.example.covidupt.ViewModels.Peticion_ayuda_Mexico;
import com.example.covidupt.ViewModels.Peticion_resumen;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link Ayuda_MexicoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Ayuda_MexicoFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public Ayuda_MexicoFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Ayuda_MexicoFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static Ayuda_MexicoFragment newInstance(String param1, String param2) {
        Ayuda_MexicoFragment fragment = new Ayuda_MexicoFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_ayuda__mexico, container, false);
    }

    private ListView listaayuda;
    private ArrayList<Fields> listaitems=new ArrayList<>();
    private ArrayList<String> urlsgaceta=new ArrayList<>();
    private Adaptadorayudamexico adaptadorayudamexico;
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listaayuda=view.findViewById(R.id.listaayudamexico);
        Llenarlista();
        listaayuda.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Uri uri = Uri.parse(urlsgaceta.get(i));
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });
    }

    private void Llenarlista() {
        servicioPeticion service = Api2.getApi(this.getActivity()).create(servicioPeticion.class);
        Call<Peticion_ayuda_Mexico> ayuda_mexicoCall=service.Ayuda_Mexico();
        ayuda_mexicoCall.enqueue(new Callback<Peticion_ayuda_Mexico>() {
            @Override
            public void onResponse(Call<Peticion_ayuda_Mexico> call, Response<Peticion_ayuda_Mexico> response) {
                Peticion_ayuda_Mexico peticion=response.body();
                if(response.body()==null){
                    Toast.makeText(getContext(),"Ocurrio un error, intentalo más tarde",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(peticion.records.size()>0){
                    Limpiar();
                    for (int i=0;i<peticion.records.size();i++){
                        listaitems.add(new Fields(peticion.getRecords().get(i).fields.fecha_publicacion_gaceta,peticion.getRecords().get(i).fields.descripcion_programa_accion_social,peticion.getRecords().get(i).fields.nombre_programa_accion_social,peticion.getRecords().get(i).fields.unidad_responsable));
                        urlsgaceta.add(peticion.getRecords().get(i).fields.enlace_a_gaceta);
                    }
                    adaptadorayudamexico=new Adaptadorayudamexico(listaitems,getContext());
                    listaayuda.setAdapter(adaptadorayudamexico);
                }else
                    Toast.makeText(getContext(),"Error",Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onFailure(Call<Peticion_ayuda_Mexico> call, Throwable t) {
                Toast.makeText(getContext(),"No podemos conectarnos :c", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void Limpiar() {
        listaayuda.setAdapter(null);
        listaitems.clear();
    }
}