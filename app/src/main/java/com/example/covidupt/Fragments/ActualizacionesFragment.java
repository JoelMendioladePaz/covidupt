package com.example.covidupt.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.covidupt.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ActualizacionesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ActualizacionesFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public ActualizacionesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ActualizacionesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ActualizacionesFragment newInstance(String param1, String param2) {
        ActualizacionesFragment fragment = new ActualizacionesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_actualizaciones, container, false);
    }

    private Button Continuar;
    private Spinner Spinner0, Spinner1, Spinner2, Spinner3, Spinner4, Spinner5, Spinner6, Spinner7;
    private TextView embarazada;
    public int contadorfinal=0;
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final String[] items;
        final String[] genero;
        embarazada=view.findViewById(R.id.textViewembarazada);
        Spinner0=view.findViewById(R.id.spinner0);
        Spinner1=view.findViewById(R.id.spinner1);
        Spinner2=view.findViewById(R.id.spinner2);
        Spinner3=view.findViewById(R.id.spinner3);
        Spinner4=view.findViewById(R.id.spinner4);
        Spinner5=view.findViewById(R.id.spinner5);
        Spinner6=view.findViewById(R.id.spinner6);
        Spinner7=view.findViewById(R.id.spinner7);
        Continuar=view.findViewById(R.id.continuar);
        genero=getResources().getStringArray(R.array.generos);
        items = getResources().getStringArray(R.array.encuesta);
        ArrayAdapter<String> adapter=new ArrayAdapter<String>(getContext(),R.layout.adaptadorspinners,items);
        ArrayAdapter<String> adaptergeneros=new ArrayAdapter<>(getContext(),R.layout.adaptadorspinners,genero);
        adapter.setDropDownViewResource(R.layout.adaptadorspinners);
        adaptergeneros.setDropDownViewResource(R.layout.adaptadorspinners);
        Spinner0.setAdapter(adaptergeneros);
        Spinner1.setAdapter(adapter);
        Spinner2.setAdapter(adapter);
        Spinner3.setAdapter(adapter);
        Spinner4.setAdapter(adapter);
        Spinner5.setAdapter(adapter);
        Spinner6.setAdapter(adapter);
        Spinner7.setAdapter(adapter);

        Spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                int contador=0;
                if(items[i].equals("Si")&&contador%2==0){
                    contadorfinal++;
                }if(items[i].equals("No")&&contador%2==1){
                    contadorfinal--;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        Spinner7.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                int contador=0;
                if(items[i].equals("Si")&&contador%2==0){
                    contadorfinal++;
                }if(items[i].equals("No")&&contador%2==1){
                    contadorfinal--;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        Continuar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String mensaje="";
                if (contadorfinal==2){
                    mensaje="contagio";
                }else if(contadorfinal==1){
                    mensaje="probable";
                }else{
                    mensaje="neutral";
                }
                Intent recomendaciones=new Intent(getActivity(),MainActivity2.class);
                recomendaciones.putExtra("estado",mensaje);
                recomendaciones.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(recomendaciones);
                getActivity().onBackPressed();
            }
        });
    }
}