package com.example.covidupt.Fragments;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.covidupt.R;

public class MainActivity2 extends AppCompatActivity {

    private TextView textViewtitulo,textView1,textView2,textView3,textView4;
    private ImageView imagenprincipal;
    private Button boton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        textView1=(TextView) findViewById(R.id.txtv1);
        textView2=(TextView)findViewById(R.id.txtv2);
        textView3=(TextView)findViewById(R.id.txtv3);
        textView4=(TextView)findViewById(R.id.txtv4);
        textViewtitulo=(TextView)findViewById(R.id.titulo);
        imagenprincipal=(ImageView) findViewById(R.id.imagenprincipal);
        boton=(Button) findViewById(R.id.boton);
        boton.setVisibility(View.INVISIBLE);
        textView1.setCompoundDrawablesWithIntrinsicBounds(R.drawable.correct, 0, 0, 0);
        textView2.setCompoundDrawablesWithIntrinsicBounds(R.drawable.correct, 0, 0, 0);
        textView3.setCompoundDrawablesWithIntrinsicBounds(R.drawable.correct, 0, 0, 0);
        textView4.setCompoundDrawablesWithIntrinsicBounds(R.drawable.correct, 0, 0, 0);
        Bundle datos=this.getIntent().getExtras();
        String estado=datos.getString("estado");
        if(estado.equals("contagio")){
            textViewtitulo.setText("Peligro de contagio.");
            textView1.setText("Es importante que busques ayuda medica lo antes posible.");
            textView2.setText("Tu traslado a un hospital debe ser con las medidas necesarias, usa cubrebocas.");
            textView3.setText("Evita ir en transporte publico y que tu viaje sea directo a un hospital.");
            textView4.setText("Selecciona el boton para ver los hospitales cerca de ti.");
            boton.setVisibility(View.VISIBLE);
            imagenprincipal.setImageResource(R.drawable.cough);
        }else if(estado.equals("probable")){
            textViewtitulo.setText("Toma las precauciones.");
            textView1.setText("Mantén sana distancia, al menos 1.5m entre cada persona.");
            textView2.setText("Evita lugares concurridos o reducidos con mucha gente.");
            textView3.setText("Quédate en casa y no tengas contacto con muchas personas.");
            textView4.setText("Recuerda que no podemos saber con certeza, si lo requieres busca un hospital cerca de ti.");
            boton.setVisibility(View.VISIBLE);
            imagenprincipal.setImageResource(R.drawable.error);
        }else {
            textViewtitulo.setText("SE PRECAVIDO.");
            textView1.setText("Mantén sana distancia, al menos 1.5m entre cada persona.");
            textView2.setText("Evita lugares concurridos o reducidos con mucha gente.");
            textView3.setText("Lava tus manos constantemente cuando toques cualquier superficie..");
            textView4.setText("Quédate en casa y si no es posible, desinfecta todo lo que traigas contigo.");
            boton.setVisibility(View.INVISIBLE);
        }
        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(MainActivity2.this,MainActivity.class);
                i.putExtra("Mapa","mapa");
                startActivity(i);
            }
        });
    }
}