package com.example.covidupt.Fragments;

import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.example.covidupt.Adaptadores.ResumenAdaptador;
import com.example.covidupt.Adaptadores.ResumenAdaptadorPais;
import com.example.covidupt.Api.Api;
import com.example.covidupt.R;
import com.example.covidupt.Servicios.servicioPeticion;
import com.example.covidupt.ViewModels.Country;
import com.example.covidupt.ViewModels.Global;
import com.example.covidupt.ViewModels.Peticion_resumen;

import java.util.ArrayList;
import java.util.List;

import lecho.lib.hellocharts.model.PieChartData;
import lecho.lib.hellocharts.model.SliceValue;
import lecho.lib.hellocharts.view.PieChartView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link Casos_mundialesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Casos_mundialesFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public Casos_mundialesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Casos_mundialesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static Casos_mundialesFragment newInstance(String param1, String param2) {
        Casos_mundialesFragment fragment = new Casos_mundialesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_casos_mundiales, container, false);
    }

    private ListView lista;
    private ResumenAdaptador adaptador;
    private ArrayList<Global>listaitems=new ArrayList<>();
    private PieChartView pieChartView,pieChartView2;
    private List pieData = new ArrayList<>();
    private List pieData2 = new ArrayList<>();
    private PieChartData pieChartData, pieChartData2;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        lista=view.findViewById(R.id.listamundial);
        pieChartView=view.findViewById(R.id.chart);
        pieChartView2=view.findViewById(R.id.aumento);
        obtenerItems();

    }

    private void obtenerItems() {
        servicioPeticion service = Api.getApi(this.getActivity()).create(servicioPeticion.class);
        Call<Peticion_resumen> resumenCall=service.Resumen();
        resumenCall.enqueue(new Callback<Peticion_resumen>() {
            @Override
            public void onResponse(Call<Peticion_resumen> call, Response<Peticion_resumen> response) {
                Peticion_resumen peticion=response.body();
                if(response.body()==null){
                    Toast.makeText(getContext(),"Ocurrio un error, intentalo más tarde",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(peticion.getGlobal()!=null){
                    Limpiar();
                    listaitems.add(new Global(peticion.getGlobal().NewConfirmed,peticion.getGlobal().TotalConfirmed,peticion.getGlobal().NewDeaths,peticion.getGlobal().TotalDeaths,peticion.getGlobal().NewRecovered,peticion.getGlobal().TotalRecovered));
                    adaptador=new ResumenAdaptador(listaitems,getContext());
                    lista.setAdapter(adaptador);
                    pieData.add(new SliceValue(peticion.getGlobal().TotalConfirmed, Color.GRAY).setLabel("Total confirmados: "+peticion.getGlobal().TotalConfirmed));
                    pieData.add(new SliceValue(peticion.getGlobal().TotalDeaths, Color.RED).setLabel("Total muertes: "+peticion.getGlobal().TotalDeaths));
                    pieData.add(new SliceValue(peticion.getGlobal().TotalRecovered, Color.BLUE).setLabel("Total recuperados: "+peticion.getGlobal().TotalRecovered));
                    pieChartData= new PieChartData(pieData);
                    pieChartData.setHasLabels(true).setValueLabelTextSize(14);
                    pieChartData.setHasCenterCircle(true).setCenterText1("estado mundial").setCenterText1FontSize(20).setCenterText1Color(Color.parseColor("#0097A7"));
                    pieChartView.setPieChartData(pieChartData);
                    final int total=100;
                    int totalcasos=peticion.getGlobal().TotalConfirmed+peticion.getGlobal().TotalDeaths+peticion.getGlobal().TotalRecovered;
                    float confirmadosporcentaje,muertosporcentaje,recuperadosporcentaje;
                    confirmadosporcentaje=(peticion.getGlobal().TotalConfirmed*total)/totalcasos;
                    muertosporcentaje=(peticion.getGlobal().TotalDeaths*total)/totalcasos;
                    recuperadosporcentaje=(peticion.getGlobal().TotalRecovered*total)/totalcasos;
                    pieData2.add(new SliceValue(confirmadosporcentaje, Color.BLUE).setLabel("Confirmados "+confirmadosporcentaje+"%"));
                    pieData2.add(new SliceValue(muertosporcentaje, Color.RED).setLabel("Muertos "+muertosporcentaje+"%"));
                    pieData2.add(new SliceValue(recuperadosporcentaje, Color.MAGENTA).setLabel("Recuperados"+recuperadosporcentaje+"%"));
                    pieChartData2= new PieChartData(pieData2);
                    pieChartData2.setHasLabels(true).setValueLabelTextSize(14);
                    pieChartData2.setHasCenterCircle(true).setCenterText1("Aumento mundial").setCenterText1FontSize(20).setCenterText1Color(Color.parseColor("#0097A7"));
                    pieChartView2.setPieChartData(pieChartData2);
                }else
                    Toast.makeText(getContext(),"Error",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<Peticion_resumen> call, Throwable t) {
                Toast.makeText(getContext(),"No podemos conectarnos :c", Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void Limpiar(){
        pieData.clear();
        pieData2.clear();
        lista.setAdapter(null);
        listaitems.clear();
    }
}