package com.example.covidupt.Adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.covidupt.R;
import com.example.covidupt.ViewModels.Global;

import java.util.ArrayList;

public class ResumenAdaptador extends BaseAdapter {

    private ArrayList<Global> lista;
    private Context contexto;

    public ResumenAdaptador(ArrayList<Global> lista, Context contexto) {
        this.lista = lista;
        this.contexto = contexto;
    }

    @Override
    public int getCount() {
        return lista.size();
    }

    @Override
    public Object getItem(int i) {
        return lista.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Global item= (Global) getItem(i);
        view = LayoutInflater.from(contexto).inflate(R.layout.adaptadorresumen,null);
        TextView Titulo=(TextView) view.findViewById(R.id.titulo);
        TextView Newconfirmed=(TextView) view.findViewById(R.id.NuevosConfirmadosresumen);
        TextView NewTotalConfirmed=(TextView) view.findViewById(R.id.Totalconfirmadosresumen);
        TextView Newdeaths=(TextView) view.findViewById(R.id.Nuevasmuertesresumen);
        TextView Totaldeaths=(TextView)view.findViewById(R.id.TotalMuertesresumen);
        TextView Newrecovered=(TextView)view.findViewById(R.id.Nuevosrehabilitadosresumen);
        TextView Totalrecovered=(TextView)view.findViewById(R.id.Totalrehabilitadosresumen);
        Titulo.setText("Resumen Global");
        Newconfirmed.setText("Nuevos confirmados: "+item.getNewConfirmed());
        NewTotalConfirmed.setText("Total confirmados: "+item.getTotalConfirmed());
        Newdeaths.setText("Nuevas muertes: "+item.getNewDeaths());
        Totaldeaths.setText("Total de muertes: "+item.getTotalDeaths());
        Newrecovered.setText("Nuevos rehabilitados: "+item.getNewRecovered());
        Totalrecovered.setText("Total rehabilitados: "+item.getTotalRecovered());
        return view;
    }
}
