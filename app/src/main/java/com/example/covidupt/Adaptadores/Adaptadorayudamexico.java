package com.example.covidupt.Adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.covidupt.R;
import com.example.covidupt.ViewModels.Fields;

import java.util.ArrayList;

public class Adaptadorayudamexico extends BaseAdapter {
    ArrayList<Fields> lista;
    Context contexto;

    public Adaptadorayudamexico(ArrayList<Fields> lista, Context contexto) {
        this.lista = lista;
        this.contexto = contexto;
    }

    @Override
    public int getCount() {
        return lista.size();
    }

    @Override
    public Object getItem(int i) {
        return lista.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Fields item= (Fields) getItem(i);
        view= LayoutInflater.from(contexto).inflate(R.layout.adaptadorayudamexico,null);
        TextView nombredelprograma =(TextView) view.findViewById(R.id.nombredelprogramamexico);
        TextView unidadresponsable=(TextView) view.findViewById(R.id.unidadresponsablemexico);
        TextView descripcion=(TextView)view.findViewById(R.id.descripcionmexico);
        TextView fecha=(TextView) view.findViewById(R.id.fechapublicacionmexico);
        nombredelprograma.setText("Nombre del programa: "+item.nombre_programa_accion_social);
        unidadresponsable.setText("Unidad responsable: "+item.unidad_responsable);
        descripcion.setText("Descripción: "+item.descripcion_programa_accion_social);
        fecha.setText("Fecha: "+item.fecha_publicacion_gaceta);
        return view;
    }
}
