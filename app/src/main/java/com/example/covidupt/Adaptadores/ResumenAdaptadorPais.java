package com.example.covidupt.Adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.covidupt.R;
import com.example.covidupt.ViewModels.Country;

import java.util.ArrayList;

public class ResumenAdaptadorPais extends BaseAdapter {
    private ArrayList<Country> lista;
    private Context contexto;

    public ResumenAdaptadorPais(ArrayList<Country> lista, Context contexto) {
        this.lista = lista;
        this.contexto = contexto;
    }

    @Override
    public int getCount() {
        return lista.size();
    }

    @Override
    public Object getItem(int i) {
        return lista.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Country item= (Country) getItem(i);
        view = LayoutInflater.from(contexto).inflate(R.layout.adaptadorresumenporpais,null);
        TextView Pais=(TextView) view.findViewById(R.id.Pais);
        TextView Newconfirmed=(TextView) view.findViewById(R.id.NuevosConfirmados);
        TextView NewTotalConfirmed=(TextView) view.findViewById(R.id.Totalconfirmados);
        TextView Newdeaths=(TextView) view.findViewById(R.id.Nuevasmuertes);
        TextView Totaldeaths=(TextView)view.findViewById(R.id.TotalMuertes);
        TextView Newrecovered=(TextView)view.findViewById(R.id.Nuevosrehabilitados);
        TextView Totalrecovered=(TextView)view.findViewById(R.id.Totalrehabilitados);
        TextView Date=(TextView)view.findViewById(R.id.fecha);
        Pais.setText("Pais: "+item.getCountry());
        Newconfirmed.setText("Nuevos confirmados: "+item.getNewConfirmed());
        NewTotalConfirmed.setText("Total confirmados: "+item.getTotalConfirmed());
        Newdeaths.setText("Nuevas muertes: "+item.getNewDeaths());
        Totaldeaths.setText("Total de muertes: "+item.getTotalDeaths());
        Newrecovered.setText("Nuevos rehabilitados: "+item.getNewRecovered());
        Totalrecovered.setText("Total rehabilitados: "+item.getTotalRecovered());
        Date.setText("Actualizacion: "+item.getDate());
        return view;
    }
}
