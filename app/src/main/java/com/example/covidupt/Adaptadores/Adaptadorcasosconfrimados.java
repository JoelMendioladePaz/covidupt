package com.example.covidupt.Adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.covidupt.R;
import com.example.covidupt.ViewModels.Day_one;

import java.util.ArrayList;
import java.util.List;

public class Adaptadorcasosconfrimados extends BaseAdapter {
    private ArrayList<Day_one> lista;
    private Context contexto;

    public Adaptadorcasosconfrimados(ArrayList<Day_one> lista, Context contexto) {
        this.lista = lista;
        this.contexto = contexto;
    }

    @Override
    public int getCount() {
        return lista.size();
    }

    @Override
    public Object getItem(int i) {
        return lista.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Day_one item=(Day_one) getItem(i);
        view= LayoutInflater.from(contexto).inflate(R.layout.adaptadorcasosconfirmados,null);
        TextView pais=(TextView) view.findViewById(R.id.Paisconfirmados);
        TextView cantidad=(TextView) view.findViewById(R.id.cantidadcasosconfirmados);
        TextView status=(TextView) view.findViewById(R.id.statusconfirmados);
        TextView fecha=(TextView) view.findViewById(R.id.fechaconfirmados);
        pais.setText("Pais: "+ item.getCountry());
        cantidad.setText("Cantidad casos: "+item.getCases());
        status.setText("estado: "+item.getStatus());
        fecha.setText("Fecha: " +item.getDate());
        return view;
    }
}
