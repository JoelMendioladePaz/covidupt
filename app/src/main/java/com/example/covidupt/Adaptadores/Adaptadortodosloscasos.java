package com.example.covidupt.Adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.covidupt.R;
import com.example.covidupt.ViewModels.Day_one_all_status;

import java.util.ArrayList;

public class Adaptadortodosloscasos extends BaseAdapter {
    private ArrayList<Day_one_all_status> lista;
    private Context contexto;

    public Adaptadortodosloscasos(ArrayList<Day_one_all_status> lista, Context contexto) {
        this.lista = lista;
        this.contexto = contexto;
    }

    @Override
    public int getCount() {
        return lista.size();
    }

    @Override
    public Object getItem(int i) {
        return lista.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Day_one_all_status item=(Day_one_all_status) getItem(i);
        view= LayoutInflater.from(contexto).inflate(R.layout.adaptadortodosloscasos,null);
        TextView pais=(TextView) view.findViewById(R.id.Paistodoscasos);
        TextView confirmados=(TextView) view.findViewById(R.id.cantidadtodosconfirmados);
        TextView muertes=(TextView) view.findViewById(R.id.cantidadtodosmuertes);
        TextView rehabilitados=(TextView) view.findViewById(R.id.cantidadtodosrehabilitados);
        TextView hopitalizados=(TextView) view.findViewById(R.id.cantidadtodoshospitalizados);
        TextView fecha=(TextView) view.findViewById(R.id.fechatodos);
        pais.setText("Pais: "+ item.Country);
        confirmados.setText("Confirmados: "+item.Confirmed);
        muertes.setText("Muertes: "+item.Deaths);
        rehabilitados.setText("Rehabilitados: "+item.Recovered);
        hopitalizados.setText("Hospitalizados: "+item.Active);
        fecha.setText("Fecha: "+item.Date);
        return view;
    }
}
