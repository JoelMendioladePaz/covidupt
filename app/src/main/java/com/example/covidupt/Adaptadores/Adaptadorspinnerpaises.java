package com.example.covidupt.Adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.covidupt.R;
import com.example.covidupt.ViewModels.Countries;

import java.util.ArrayList;
import java.util.List;

public class Adaptadorspinnerpaises extends BaseAdapter {
    private ArrayList<Countries> lista;
    private Context contexto;

    public Adaptadorspinnerpaises(ArrayList<Countries> lista, Context contexto) {
        this.lista = lista;
        this.contexto = contexto;
    }

    @Override
    public int getCount() {
        return lista.size();
    }

    @Override
    public Object getItem(int i) {
        return lista.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view= LayoutInflater.from(contexto).inflate(R.layout.spinner_personalizado,null);
        TextView descripcion=(TextView) view.findViewById(R.id.paisesspinner);
        descripcion.setText(lista.get(i).Country);
        return view;
    }
}
